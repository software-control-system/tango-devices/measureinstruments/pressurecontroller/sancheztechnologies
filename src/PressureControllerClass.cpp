static const char *ClassId    = "$Id:  $";
static const char *TagName    = "$Name:  $";
static const char *CvsPath    = "$Source:  $";
static const char *SvnPath    = "$HeadURL: $";
static const char *HttpServer = "http://www.esrf.fr/computing/cs/tango/tango_doc/ds_doc/";
//+=============================================================================
//
// file :        PressureControllerClass.cpp
//
// description : C++ source for the PressureControllerClass. A singleton
//               class derived from DeviceClass. It implements the
//               command list and all properties and methods required
//               by the PressureController once per process.
//
// project :     TANGO Device Server
//
// $Author:  $
//
// $Revision:  $
//
// $Log:  $
//
// copyleft :   European Synchrotron Radiation Facility
//              BP 220, Grenoble 38043
//              FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>

#include <PressureController.h>
#include <PressureControllerClass.h>


//+----------------------------------------------------------------------------
/**
 *	Create PressureControllerClass singleton and return it in a C function for Python usage
 */
//+----------------------------------------------------------------------------
extern "C" {
#ifdef WIN32

__declspec(dllexport)

#endif

	Tango::DeviceClass *_create_PressureController_class(const char *name) {
		return PressureController_ns::PressureControllerClass::init(name);
	}
}


namespace PressureController_ns
{
//+----------------------------------------------------------------------------
//
// method : 		EmergencyClass::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *EmergencyClass::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "EmergencyClass::execute(): arrived" << endl;

	((static_cast<PressureController *>(device))->emergency());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		GotoAmbiantClass::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *GotoAmbiantClass::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "GotoAmbiantClass::execute(): arrived" << endl;

	((static_cast<PressureController *>(device))->goto_ambiant());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		StopClass::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *StopClass::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "StopClass::execute(): arrived" << endl;

	((static_cast<PressureController *>(device))->stop());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		StartRegulationClass::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *StartRegulationClass::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "StartRegulationClass::execute(): arrived" << endl;

	((static_cast<PressureController *>(device))->start_regulation());
	return new CORBA::Any();
}


//+----------------------------------------------------------------------------
//
// method : 		RawReadCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *RawReadCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "RawReadCmd::execute(): arrived" << endl;

	Tango::DevString	argin;
	extract(in_any, argin);

	return insert((static_cast<PressureController *>(device))->raw_read(argin));
}

//+----------------------------------------------------------------------------
//
// method : 		RawWriteCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *RawWriteCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "RawWriteCmd::execute(): arrived" << endl;

	Tango::DevString	argin;
	extract(in_any, argin);

	return insert((static_cast<PressureController *>(device))->raw_write(argin));
}


//
//----------------------------------------------------------------
//	Initialize pointer for singleton pattern
//----------------------------------------------------------------
//
PressureControllerClass *PressureControllerClass::_instance = NULL;

//+----------------------------------------------------------------------------
//
// method : 		PressureControllerClass::PressureControllerClass(string &s)
// 
// description : 	constructor for the PressureControllerClass
//
// in : - s : The class name
//
//-----------------------------------------------------------------------------
PressureControllerClass::PressureControllerClass(string &s):DeviceClass(s)
{

	cout2 << "Entering PressureControllerClass constructor" << endl;
	set_default_property();
	get_class_property();
	write_class_property();
	
	cout2 << "Leaving PressureControllerClass constructor" << endl;

}
//+----------------------------------------------------------------------------
//
// method : 		PressureControllerClass::~PressureControllerClass()
// 
// description : 	destructor for the PressureControllerClass
//
//-----------------------------------------------------------------------------
PressureControllerClass::~PressureControllerClass()
{
	_instance = NULL;
}

//+----------------------------------------------------------------------------
//
// method : 		PressureControllerClass::instance
// 
// description : 	Create the object if not already done. Otherwise, just
//			return a pointer to the object
//
// in : - name : The class name
//
//-----------------------------------------------------------------------------
PressureControllerClass *PressureControllerClass::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			string s(name);
			_instance = new PressureControllerClass(s);
		}
		catch (bad_alloc)
		{
			throw;
		}		
	}		
	return _instance;
}

PressureControllerClass *PressureControllerClass::instance()
{
	if (_instance == NULL)
	{
		cerr << "Class is not initialised !!" << endl;
		exit(-1);
	}
	return _instance;
}

//+----------------------------------------------------------------------------
//
// method : 		PressureControllerClass::command_factory
// 
// description : 	Create the command object(s) and store them in the 
//			command list
//
//-----------------------------------------------------------------------------
void PressureControllerClass::command_factory()
{
	command_list.push_back(new RawReadCmd("RawRead",
		Tango::DEV_STRING, Tango::DEV_STRING,
		" full mnemonic to send (including address)",
		"the controller' response",
		Tango::OPERATOR));
	command_list.push_back(new RawWriteCmd("RawWrite",
		Tango::DEV_STRING, Tango::DEV_STRING,
		" full mnemonic/value to send (including address)",
		"the controller' response",
		Tango::OPERATOR));
	command_list.push_back(new StartRegulationClass("StartRegulation",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new StopClass("Stop",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new GotoAmbiantClass("GotoAmbiant",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new EmergencyClass("Emergency",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));

	//	add polling if any
	for (unsigned int i=0 ; i<command_list.size(); i++)
	{
	}
}

//+----------------------------------------------------------------------------
//
// method : 		PressureControllerClass::get_class_property
// 
// description : 	Get the class property for specified name.
//
// in :		string	name : The property name
//
//+----------------------------------------------------------------------------
Tango::DbDatum PressureControllerClass::get_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		PressureControllerClass::get_default_device_property()
// 
// description : 	Return the default value for device property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum PressureControllerClass::get_default_device_property(string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//+----------------------------------------------------------------------------
//
// method : 		PressureControllerClass::get_default_class_property()
// 
// description : 	Return the default value for class property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum PressureControllerClass::get_default_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		PressureControllerClass::device_factory
// 
// description : 	Create the device object(s) and store them in the 
//			device list
//
// in :		Tango::DevVarStringArray *devlist_ptr : The device name list
//
//-----------------------------------------------------------------------------
void PressureControllerClass::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{

	//	Create all devices.(Automatic code generation)
	//-------------------------------------------------------------
	for (unsigned long i=0 ; i < devlist_ptr->length() ; i++)
	{
		cout4 << "Device name : " << (*devlist_ptr)[i].in() << endl;
						
		// Create devices and add it into the device list
		//----------------------------------------------------
		device_list.push_back(new PressureController(this, (*devlist_ptr)[i]));							 

		// Export device to the outside world
		// Check before if database used.
		//---------------------------------------------
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(device_list.back());
		else
			export_device(device_list.back(), (*devlist_ptr)[i]);
	}
	//	End of Automatic code generation
	//-------------------------------------------------------------

}
//+----------------------------------------------------------------------------
//	Method: PressureControllerClass::attribute_factory(vector<Tango::Attr *> &att_list)
//-----------------------------------------------------------------------------
void PressureControllerClass::attribute_factory(vector<Tango::Attr *> &att_list)
{
	//	Attribute : pressure
	pressureAttrib	*pressure = new pressureAttrib();
	Tango::UserDefaultAttrProp	pressure_prop;
	pressure_prop.set_label("Pressure");
	pressure_prop.set_description("read the pressure on the controller (mnemo 1PV)");
	pressure->set_default_properties(pressure_prop);
	att_list.push_back(pressure);

	//	Attribute : pressureSetpoint
	pressureSetpointAttrib	*pressure_setpoint = new pressureSetpointAttrib();
	Tango::UserDefaultAttrProp	pressure_setpoint_prop;
	pressure_setpoint_prop.set_label("Pressure Setpoint");
	pressure_setpoint_prop.set_description("read/write the pressure setpoint on the controller (mnemo 1SP)");
	pressure_setpoint->set_default_properties(pressure_setpoint_prop);
	att_list.push_back(pressure_setpoint);

	//	Attribute : pressureRamp
	pressureRampAttrib	*pressure_ramp = new pressureRampAttrib();
	Tango::UserDefaultAttrProp	pressure_ramp_prop;
	pressure_ramp_prop.set_label("PressureRamp");
	pressure_ramp_prop.set_description("read/write the pressure ramp on the controller (mnemo 1RP)");
	pressure_ramp->set_default_properties(pressure_ramp_prop);
	pressure_ramp->set_disp_level(Tango::EXPERT);
	att_list.push_back(pressure_ramp);

	//	Attribute : maxSetpoint
	maxSetpointAttrib	*max_setpoint = new maxSetpointAttrib();
	Tango::UserDefaultAttrProp	max_setpoint_prop;
	max_setpoint_prop.set_label("Max Pressure setpoint");
	max_setpoint_prop.set_description("read/write the MAX pressure setpoint on the controller (mnemo 1SP)");
	max_setpoint->set_default_properties(max_setpoint_prop);
	att_list.push_back(max_setpoint);

	//	Attribute : pressureAlarmHigh
	pressureAlarmHighAttrib	*pressure_alarm_high = new pressureAlarmHighAttrib();
	Tango::UserDefaultAttrProp	pressure_alarm_high_prop;
	pressure_alarm_high_prop.set_label("Pressure Alarm High");
	pressure_alarm_high_prop.set_description("read/write the pressure Alarm High on the controller (mnemo 1PV)");
	pressure_alarm_high->set_default_properties(pressure_alarm_high_prop);
	att_list.push_back(pressure_alarm_high);

	//	End of Automatic code generation
	//-------------------------------------------------------------
}


//+----------------------------------------------------------------------------
//
// method : 		PressureControllerClass::get_class_property()
// 
// description : 	Read the class properties from database.
//
//-----------------------------------------------------------------------------
void PressureControllerClass::get_class_property()
{
	//	Initialize your default values here (if not done with  POGO).
	//------------------------------------------------------------------

	//	Read class properties from database.(Automatic code generation)
	//------------------------------------------------------------------

	//	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		get_db_class()->get_property(cl_prop);
	Tango::DbDatum	def_prop;
	int	i = -1;


	//	End of Automatic code generation
	//------------------------------------------------------------------

}

//+----------------------------------------------------------------------------
//
// method : 	PressureControllerClass::set_default_property
// 
// description: Set default property (class and device) for wizard.
//              For each property, add to wizard property name and description
//              If default value has been set, add it to wizard property and
//              store it in a DbDatum.
//
//-----------------------------------------------------------------------------
void PressureControllerClass::set_default_property()
{
	string	prop_name;
	string	prop_desc;
	string	prop_def;

	vector<string>	vect_data;
	//	Set Default Class Properties
	//	Set Default Device Properties
	prop_name = "Adress";
	prop_desc = "RS422 Adress for the MCAPC";
	prop_def  = "001";
	vect_data.clear();
	vect_data.push_back("001");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "SerialProxy";
	prop_desc = "Device proxy on serial line";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

}
//+----------------------------------------------------------------------------
//
// method : 		PressureControllerClass::write_class_property
// 
// description : 	Set class description as property in database
//
//-----------------------------------------------------------------------------
void PressureControllerClass::write_class_property()
{
	//	First time, check if database used
	//--------------------------------------------
	if (Tango::Util::_UseDb == false)
		return;

	Tango::DbData	data;
	string	classname = get_name();
	string	header;
	string::size_type	start, end;

	//	Put title
	Tango::DbDatum	title("ProjectTitle");
	string	str_title("PressureController");
	title << str_title;
	data.push_back(title);

	//	Put Description
	Tango::DbDatum	description("Description");
	vector<string>	str_desc;
	str_desc.push_back("control of Sanchez Tech APD pressure controller for Diamond Anvil CEll application");
	description << str_desc;
	data.push_back(description);
		
	//	put cvs or svn location
	string	filename(classname);
	filename += "Class.cpp";
	
	// Create a string with the class ID to
	// get the string into the binary
	string	class_id(ClassId);
	
	// check for cvs information
	string	src_path(CvsPath);
	start = src_path.find("/");
	if (start!=string::npos)
	{
		end   = src_path.find(filename);
		if (end>start)
		{
			string	strloc = src_path.substr(start, end-start);
			//	Check if specific repository
			start = strloc.find("/cvsroot/");
			if (start!=string::npos && start>0)
			{
				string	repository = strloc.substr(0, start);
				if (repository.find("/segfs/")!=string::npos)
					strloc = "ESRF:" + strloc.substr(start, strloc.length()-start);
			}
			Tango::DbDatum	cvs_loc("cvs_location");
			cvs_loc << strloc;
			data.push_back(cvs_loc);
		}
	}
	// check for svn information
	else
	{
		string	src_path(SvnPath);
		start = src_path.find("://");
		if (start!=string::npos)
		{
			end = src_path.find(filename);
			if (end>start)
			{
				header = "$HeadURL: ";
				start = header.length();
				string	strloc = src_path.substr(start, (end-start));
				
				Tango::DbDatum	svn_loc("svn_location");
				svn_loc << strloc;
				data.push_back(svn_loc);
			}
		}
	}

	//	Get CVS or SVN revision tag
	
	// CVS tag
	string	tagname(TagName);
	header = "$Name: ";
	start = header.length();
	string	endstr(" $");
	
	end   = tagname.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strtag = tagname.substr(start, end-start);
		Tango::DbDatum	cvs_tag("cvs_tag");
		cvs_tag << strtag;
		data.push_back(cvs_tag);
	}
	
	// SVN tag
	string	svnpath(SvnPath);
	header = "$HeadURL: ";
	start = header.length();
	
	end   = svnpath.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strloc = svnpath.substr(start, end-start);
		
		string tagstr ("/tags/");
		start = strloc.find(tagstr);
		if ( start!=string::npos )
		{
			start = start + tagstr.length();
			end   = strloc.find(filename);
			string	strtag = strloc.substr(start, end-start-1);
			
			Tango::DbDatum	svn_tag("svn_tag");
			svn_tag << strtag;
			data.push_back(svn_tag);
		}
	}

	//	Get URL location
	string	httpServ(HttpServer);
	if (httpServ.length()>0)
	{
		Tango::DbDatum	db_doc_url("doc_url");
		db_doc_url << httpServ;
		data.push_back(db_doc_url);
	}

	//  Put inheritance
	Tango::DbDatum	inher_datum("InheritedFrom");
	vector<string> inheritance;
	inheritance.push_back("Device_4Impl");
	inher_datum << inheritance;
	data.push_back(inher_datum);

	//	Call database and and values
	//--------------------------------------------
	get_db_class()->put_property(data);
}

}	// namespace
